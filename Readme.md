# Variabilite du PainlesS

Project : Project final @PSTL
Date :Juin 2020
\
*Requirement :*
c++ \
FeatureIDE

*Le projet contient les fichiers suivants :*\
1- configs : ce dossier contient la configuration de notre projet .\
2- features :contient les differents features.\
3- products :contient les variabilités generées (16 differents products)\
4- src:/
5-model.xml : c'est le modele de notre projet.
\
pour plus d'info regardez le rapport.